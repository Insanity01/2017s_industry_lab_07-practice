package ictgradschool.industry.lab07.ex02;

import ictgradschool.Keyboard;

/**
 * A guessing game!
 */
public class GuessingGame {

    /**
     * Plays the actual guessing game.
     * <p>
     * You shouldn't need to edit this method for this exercise.
     */
    public void start() {

        int number = getRandomValue();
        int guess = 0;


        while (guess != number) {
            try {
                System.out.println(number);
                guess = getUserGuess();
                if (guess > number) {
                    System.out.println("Too high!");
                } else if (guess < number) {
                    System.out.println("Too low!");
                } else {
                    System.out.println("Perfect!");
                }
            } catch (NumberFormatException e) {
                System.out.println("You have not entered a number!");
            } catch (IllegalArgumentException e) {
                System.out.println("Your number needs to be between 1 and 100");
            }
        }

    }

    /**
     * Gets a random integer between 1 and 100.
     * <p>
     * You shouldn't need to edit this method for this exercise.
     */
    private int getRandomValue() {

        return (int) (Math.random() * 100) + 1;
    }

    /**
     * Gets the user's guess from the keyboard. Currently assumes that the user will always enter a valid guess.
     * <p>
     * TODO Implement some error handling, for the cases where the user enters a value that's too big, too small, or
     * TODO not an integer. Change this method so it's guaranteed to return an integer between 1 & 100, inclusive.
     */
    private int getUserGuess() throws NumberFormatException {
        System.out.print("Enter your guess: ");

        int input = Integer.parseInt(Keyboard.readInput());

        if (input > 100 || input < 1) {
            throw new IllegalArgumentException();
        }

        return input;

    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        GuessingGame ex = new GuessingGame();
        ex.start();

    }
}
